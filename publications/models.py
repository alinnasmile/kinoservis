# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models


class Publication (models.Model):
    title = models.CharField(max_length=100)
    text = models.TextField(max_length=5000)
    #created_by =
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title


# Create your models here.
