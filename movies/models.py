# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from dynamic_filenames import FilePattern


class Crew(models.Model):
    url = models.CharField(max_length=255)
    full_name_uk = models.CharField(max_length=100)
    full_name_eng = models.CharField(max_length=100, null=True, blank=True)
    date_of_birth = models.CharField(max_length=100, null=True, blank=True)
    place_of_birth = models.CharField(max_length=255, null=True, blank=True)
    height = models.PositiveSmallIntegerField(null=True, blank=True)

    def __str__(self):
        return self.full_name_eng


class Movie(models.Model):
    name_uk = models.CharField(max_length=100)
    name_eng = models.CharField(max_length=100)
    slogan = models.CharField(max_length=200, null=True, blank=True)
    genre = models.ManyToManyField('Genre')
    country = models.ManyToManyField('Country')
    budget = models.PositiveIntegerField(null=True, blank=True)
    #budget_currency = models.CharField(max_length=50)
    premiere = models.DateField(null=True, blank=True)
    premiere_ukr = models.DateField(null=True, blank=True)
    #year = models.PositiveIntegerField()
    box_office_ukr = models.PositiveIntegerField(null=True, blank=True) #кассовые сборы в Украине
    #box_office_ukr_currency = models.CharField(max_length=50)
    box_office_world = models.PositiveIntegerField(null=True, blank=True) #кассовые сборы в мире
    #box_office_world_currency = models.CharField(max_length=50)
    #producer_of = models.ManyToManyField('ProducerOf') #производитель
    time = models.PositiveIntegerField() #продолжительность
    story = models.TextField(max_length=5000) #сюжет
    #image = models.ForeignKey('MovieImage', on_delete=models.CASCADE)
    crew = models.ManyToManyField(Crew, through='movies.CrewToMovie', null=True, blank=True)

    def __str__(self):
        return self.name_uk


CREW_ROLE = (
    (0, 'actor'),
    (1, 'director'),
    (2, 'scenarist')
)


class CrewToMovie(models.Model):
    crew = models.ForeignKey(Crew, on_delete=models.CASCADE)
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)
    role = models.PositiveSmallIntegerField(choices=CREW_ROLE, db_index=True)


class Country(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Genre(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


#class ProducerOf(models.Model):
    #name = models.CharField(max_length=100)

    #def __str__(self):
        #return self.name


upload_to_pattern = FilePattern(
    filename_pattern='{app_name:.25}/{model_name:.30}/{uuid:base32}{ext}'
) #доделать картинки - разбить по папкам

IMAGE_TYPE = (
    (0, 'poster'),
    (1, 'frame'),
)


class MovieImage(models.Model):
    image = models.ImageField(upload_to='movies')
    image_type = models.PositiveSmallIntegerField(choices=IMAGE_TYPE, db_index=True)
    movie = models.ForeignKey('Movie', on_delete=models.CASCADE)


class ParseUrl (models.Model):
    title = models.CharField(max_length=100)
    title_eng = models.CharField(max_length=100)
    url = models.CharField(max_length=1000)
    is_parse = models.BooleanField(default=True)
