from movies.models import ParseUrl, Movie
from movies.models import Genre, Country, Crew, CREW_ROLE, CrewToMovie
from django.core.management.base import BaseCommand, CommandError
from movies.management.commands.utils.parser_m import parser_movies


CREW_ROLE = {
    'actor': 0,
    'director': 1,
    'scenarist': 2,
}


class Command(BaseCommand):

    def handle(self, *args, **options):
        m = ParseUrl.objects.all()
        for url_item in m:
            link = 'https://kinobaza.com.ua{}'.format(url_item.url)
            movie_dict = parser_movies(link, url_item.title, url_item.title_eng)
            movie_db = Movie.objects.create(
                name_uk=movie_dict['name'],
                name_eng=movie_dict['orig_name'],
                slogan=movie_dict['slogan'],
                budget=movie_dict['budget'],
                premiere=movie_dict['year'],
                premiere_ukr=movie_dict['year_ukr'],
                box_office_world=movie_dict['box_office_world'],
                box_office_ukr=movie_dict['box_office_ukr'],
                time=movie_dict['time'],
                story=movie_dict['overview']
            )

            genres_list = movie_dict['genres']
            for genre_name in genres_list:
                count_exist_genre = Genre.objects.filter(name=genre_name).count()
                if count_exist_genre == 0:
                    g = Genre.objects.create(name=genre_name)
                else:
                    g = Genre.objects.get(name=genre_name)
                    movie_db.genre.add(g)

            countries_list = movie_dict['countries']
            for country_name in countries_list:
                count_exist_country = Country.objects.filter(name=country_name).count()
                if count_exist_country == 0:
                    c = Country.objects.create(name=country_name)
                else:
                    c = Country.objects.get(name=country_name)
                    movie_db.country.add(c)

            crew_list = movie_dict['crew']
            for crew_member in crew_list:
                count_exist_crew = Crew.objects.filter(url=crew_member['url']).count()
                if count_exist_crew == 0:
                    crew = Crew.objects.create(url=crew_member['url'], full_name_uk=crew_member['name'])
                else:
                    crew = Crew.objects.get(url=crew_member['url'])
                role = CREW_ROLE[crew_member['role']]
                CrewToMovie.objects.create(crew=crew, movie=movie_db, role=role)





            #for item in movies['genres']:
                #is_exist=Genre.objects.filter(name=item).count()
                #if is_exist == 0:
                    #g = Genre.objects.create(name=item)
                    #print(g)
            #for item in movies['countries']:
                #is_exist=Country.objects.filter(name=item).count()
                #if is_exist == 0:
                    #c = Country.objects.create(name=item)
                    #print(c)





