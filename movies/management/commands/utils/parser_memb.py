import requests
from bs4 import BeautifulSoup


def parser_member(url):
    r = requests.get(url)
    html_doc = r.text
    soup = BeautifulSoup(html_doc, 'html.parser')
    member = {}
    member_block = soup.find('div', attrs={'class': 'row details-panel'}).find('div', attrs={'class': 'col-md-8 col-sm-12'})
    date = None
    place = None
    height = None
    name = member_block.find('h1', attrs={'class': 'title__movie'}).text
    name_orig_block = member_block.find('h2', attrs={'class': 'altname'})
    name_orig = None
    if name_orig_block is not None:
        name_orig = name_orig_block.text
    for element in member_block.find('table').find_all('tr'):
        cell = element.find_all('td')
        if 'Дата народження' in cell[0].text:
            date = cell[1].text
            date = date.split('(')[0]
            print(date)
        elif 'Місце народження' in cell[0].text:
            place = cell[1].text
            #print(place)
        elif 'Зріст' in cell[0].text:
            height = cell[1].text.replace('\n', '')
            height = int(''.join(filter(str.isdigit, height)) or 0)
            #print(height)
    #print(name)
    print(name_orig)

    member['name'] = name
    member['name_orig'] = name_orig
    member['date'] = date
    member['place'] = place
    member['height'] = height
    return member

