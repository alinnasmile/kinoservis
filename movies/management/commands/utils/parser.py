import requests
from bs4 import BeautifulSoup


def parse_url(url):
  r = requests.get(url)
  html_doc = r.text
  inf_of_movies_list = []
  soup = BeautifulSoup(html_doc, 'html.parser')
  for link in soup.select('.row.movie_item.js-movie-item'):
      url_page = link.find('h3', attrs={'class': 'media-heading'}).find('a').get('href') # наш адрес
      title = link.find('h3', attrs={'class': 'media-heading'}).find('a').text # наше название
      title_eng = link.find('div', attrs={'class': 'text-muted mb5'}).text
      title_eng = title_eng.replace('\n', '')
      d = {
          'title_eng': title_eng, # даем названия переменнім парсера ключ - значение.for
          'title': title,  #справа имена переменніх с парсера
          'url_page': url_page
      }
      inf_of_movies_list.append(
          d
      )

  return inf_of_movies_list






