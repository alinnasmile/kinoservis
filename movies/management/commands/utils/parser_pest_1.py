from bs4 import BeautifulSoup
import requests

def parser(url):
    #url = 'http://pesticidov.net/chem/asb-gryunland__himinvest-tov/2070-asb-greenworld/'
    r = requests.get(url)
    html_doc = r.text
    soup = BeautifulSoup(html_doc, 'html.parser')

    data = {}

    block = soup.find('div', attrs={'class': 'item_content'})
    name = block.find('h1', attrs={'class': 'item_name'}).text
    name = name.lstrip()
    print(name)

    block_item = soup.find('div', attrs={'class': 'item_info'}).find_all('div', attrs={'class': 'item_info_row'})
    for element in block_item:
        key = element.find('b').text
        key = key.lstrip()
        value = element.find('span').text
        value = value.lstrip()
        value = value.rstrip()
        data[key] = value
    print(data)

    table = block.find_all('table')

    for table in block.find_all('table'):
        rows = table.find_all('tr')
        first_row = rows[0].find_all('td')
        if 'Діюча речовина та її вміст' in first_row[0].text:
            for row in rows[1:]:
                cells = row.find_all('td')
                print(first_row[0].text.strip(), cells[0].text.strip())
                print(first_row[1].text.strip(), cells[1].text.strip())
        if 'Норма витрати препарату,' in first_row[0].text:
            for row in rows[1:]:
                cells = row.find_all('td')
                print(first_row[0].text.strip(), cells[0].text.strip())
                print(first_row[1].text.strip(), cells[1].text.replace('\n', '').strip())
                print(first_row[2].text.strip(), cells[2].text.strip())
                print(first_row[3].text.strip(), cells[3].text.strip())
                print(first_row[4].text.strip(), cells[4].text.strip())
                print(first_row[5].text.strip(), cells[5].text.strip())


f = open('url_pest.txt')
lines = f.readlines()
for line in lines[:5]:
    print(line)
    parser(line)

