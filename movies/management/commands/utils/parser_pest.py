from bs4 import BeautifulSoup
import requests

file = open('url_pest.txt', 'w')

for i in range(0, 5):
    if i == 0:
        url = "http://pesticidov.net/pesticidu/dobrivo"
    else:
        url = ('http://pesticidov.net/pesticidu/dobrivo/{}/'.format(i*10))
        r = requests.get(url)
        html_doc = r.text
        soup = BeautifulSoup(html_doc, 'html.parser')

        link_block = soup.find('div', attrs={'class': 'content'}).find('div', attrs={'class': 'main'}).find('ul', attrs={'class': 'item_list'})
        for block in link_block.find_all('li'):
            link = block.find('a').get('href')
            link = "{}\n".format(link)
            file.write(link)

file.close()

