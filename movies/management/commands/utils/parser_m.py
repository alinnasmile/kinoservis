import requests
from bs4 import BeautifulSoup
import dateparser



def parser_movies(url, name, orig_name):
    print(name, orig_name)
    r = requests.get(url)
    html_doc = r.text
    movie = {}
    soup = BeautifulSoup(html_doc, 'html5lib')

    movie_block = soup.find('div', attrs={'class': 'row js-movie-item'})
    genres = []
    countries = [] #объявляю переменную списком по умолчанию (если переменной не будет, то будет пустой список и не будет ошибки)
    year = ''
    #directors = []
    #scenarists = []
    slogan = None
    year_ukr = None
    budget = None
    box_office_world = None
    box_office_ukr = None
    time = ''
    crew = []
    for row in movie_block.find(attrs={'class': 'row details-panel'}).find('table').find_all('tr'):
        cell = row.find_all('td')
        if 'Жанр' in cell[0].text:
            for g in (cell[1].find_all('a')):
                genres.append(g.text)
            #print(genres)
        elif 'Країни' in cell[0].text:
            for g in (cell[1].find_all('a')):
                countries.append(g.text)
            #print(countries)
        elif "Прем'єра в світі" in cell[0].text:
            year = cell[1].text.replace('\n', '')
            year = dateparser.parse(year)
            #print(datetime_object)
            #print(year)
        elif 'Режисер' in cell[0].text:
            for g in (cell[1].find_all('a')):
                crew.append({
                    'name': g.text,
                    'url': g['href'],
                    'role': 'director'
                })
        elif 'Сценарист' in cell[0].text:
            for g in (cell[1].find_all('a')):
                crew.append({
                    'name': g.text,
                    'url': g['href'],
                    'role': 'scenarist'
                })
        elif 'Гасло' in cell[0].text:
            slogan = cell[1].text.replace('\n', '')
            #print(slogan)
        elif "Прем'єра в Україні" in cell[0].text:
            year_ukr = cell[1].text.replace('\n', '')
            year_ukr = dateparser.parse(year_ukr)
            #print(datetime_object)
            #print(year_ukr)
        elif 'Бюджет' in cell[0].text:
            budget = cell[1].text.replace('\n', '')
            budget = budget.split('$')[0]
            budget = int(''.join(filter(str.isdigit, budget)) or 0)
            #print(budget)
        elif 'Касові збори в світі' in cell[0].text:
            box_office_world = cell[1].text.replace('\n', '')
            box_office_world = box_office_world.split('$')[0]
            box_office_world = int(''.join(filter(str.isdigit, box_office_world)) or 0)
            #print(box_office_world)
        elif 'Касові збори в Україні' in cell[0].text:
            box_office_ukr = cell[1].text.replace('\n', '')
            box_office_ukr = box_office_ukr.split('$')[0]
            box_office_ukr = int(''.join(filter(str.isdigit, box_office_ukr)) or 0)
            #print(box_office_ukr)
        elif 'Тривалість' in cell[0].text:
            time = cell[1].text.replace('\n', '')
            time = time.split('хвилин')[0]
            #print(time)

    overview = soup.find('p', attrs={'class':'overview well'}).text
    #print(overview)

    #actors_list = []
    actors_block = soup.find('div', attrs={'class': 'actors-container clearfix'})
    actors = actors_block.find_all('div', attrs={'class': 'media-body'})
    for actor in actors:
        url = actor.find('b', attrs={'class': 'media-heading cast__name'}).find('a').get('href')
        actor_name = actor.find('b', attrs={'class': 'media-heading cast__name'}).find('a').text
        crew.append({
            'url': url,
            'name': actor_name,
            'role': 'actor'
        })
    #print(actors_list)

    movie['name'] = name
    movie['orig_name'] = orig_name
    movie['genres'] = genres
    movie['countries'] = countries
    movie['year'] = year
    movie['year_ukr'] = year_ukr
    movie['slogan'] = slogan
    movie['budget'] = budget
    movie['box_office_world'] = box_office_world
    movie['box_office_ukr'] = box_office_ukr
    movie['time'] = time
    movie['overview'] = overview
    movie['crew'] = crew
    return movie






