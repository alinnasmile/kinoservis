from django.core.management.base import BaseCommand, CommandError
from movies.management.commands.utils.parser_memb import parser_member
from movies.models import Crew


class Command(BaseCommand):

    def handle(self, *args, **options):
        m = Crew.objects.all()
        for crew_item in m:
            link = 'https://kinobaza.com.ua{}'.format(crew_item.url)
            print(link)
            memb_dict = parser_member(link)

            crew_item.full_name_uk = memb_dict['name']
            crew_item.full_name_eng = memb_dict['name_orig']
            crew_item.date_of_birth = memb_dict['date']
            crew_item.place_of_birth = memb_dict['place']
            crew_item.height = memb_dict['height']
            crew_item.save()
