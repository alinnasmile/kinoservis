from django.core.management.base import BaseCommand, CommandError
from movies.models import ParseUrl
from movies.management.commands.utils.parser import parse_url


class Command(BaseCommand):

    def handle(self, *args, **options):
        url = 'https://kinobaza.com.ua/titles?page=5'
        inf_of_movies_list = parse_url(url)
        for item in inf_of_movies_list:
            # какому атрибуту (полю) модели какой єлмент списка соответсвует
            p = ParseUrl.objects.create(
                title=item['title'],
                title_eng=item['title_eng'],
                url=item['url_page']
            )
            print('Заголовок фильма', item['title'])
            print('Номер записи в БД', p.pk)
