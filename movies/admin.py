# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from movies.models import Movie, Crew, CrewToMovie, Country, Genre, MovieImage, ParseUrl

class CrewToMovieAdminInline(admin.StackedInline):
    model = CrewToMovie

class MovieImageAdminInline(admin.StackedInline):
    model = MovieImage

class MovieAdmin(admin.ModelAdmin):
    inlines = [CrewToMovieAdminInline, MovieImageAdminInline]





admin.site.register(Movie, MovieAdmin)
admin.site.register(Crew)
admin.site.register(Country)
admin.site.register(Genre)
#admin.site.register(ProducerOf)
admin.site.register(MovieImage)
admin.site.register(ParseUrl)

