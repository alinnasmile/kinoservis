from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.movies_list, name='movies_list'),
    url(r'^movie/(?P<pk>[0-9]+)/$', views.movie_detail, name='movie_detail')
]